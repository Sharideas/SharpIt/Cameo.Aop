﻿using System;
using System.Runtime.Remoting.Messaging;
using Cameo.Aop;

namespace TestRunner
{
    public class Program
    {
        static void Main(string[] args)
        {
            var t = new Test();
            t.Check = 3456;
            t.TTTTT();
            Console.ReadKey();
        }
    }

    [SinkContext]
    public class Test : ContextBoundObject, IHasLogger
    {
        private class L : ICustomLogger
        {
            public L()
            {
                DoBeforeMethodCall = (call, key) => { Console.WriteLine($"Before {key}"); };
                DoAfterMethodCall = (@return, key) => { Console.WriteLine($"After {key}"); };
                DoWhenThrowExcception = (@return, key) => { Console.WriteLine($"Exception {key}"); };
            }

            public Action<IMethodCallMessage, string> DoBeforeMethodCall { get; }
            public Action<IMethodReturnMessage, string> DoWhenThrowExcception { get; }
            public Action<IMethodReturnMessage, string> DoAfterMethodCall { get; }

            public void Log(string content, LogLevel level)
            {
                Console.WriteLine(content);
            }

            public void Log(string content)
            {
                Console.WriteLine(content);
            }
        }

        public ICustomLogger Logger { get; } = new L();

        [SinkMethod(SinkContent.Log | SinkContent.Verify, LogKey = "This is  a  lllllloooooooggggg")]
        public void TTTTT()
        {
            var c = Check;
            Console.WriteLine(c);
        }

        public int Check
        {
            [SinkMethod(SinkContent.Log)]
            get;
            [SinkMethod(SinkContent.Log)]
            set;
        } = 100;

        private void TestMethod(IMethodCallMessage call, string key)
        {

        }
    }
}
