﻿namespace Cameo.Aop
{
    public interface IHasVerifier
    {
        ICustomVerifier Verifier { get; }
    }

    public interface ICustomVerifier : ICustomPart
    {
        bool Verify(object content);
    }
}
