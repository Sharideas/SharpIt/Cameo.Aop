﻿namespace Cameo.Aop
{
    public interface IHasExtension
    {
        ICustomExtension Extension { get; }
    }

    public interface ICustomExtension : ICustomPart
    {
    }
}