﻿using System;
using System.Runtime.Remoting.Messaging;

namespace Cameo.Aop
{
    public interface ICustomPart
    {
        Action<IMethodCallMessage, string> DoBeforeMethodCall { get; }
        Action<IMethodReturnMessage, string> DoWhenThrowExcception { get; }
        Action<IMethodReturnMessage, string> DoAfterMethodCall { get; }
    }
}
