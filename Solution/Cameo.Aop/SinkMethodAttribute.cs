﻿using System;

namespace Cameo.Aop
{
    /// <summary>
    /// A method or a property, of a type which inherits <see cref="ContextBoundObject"/> and decorated with <see cref="SinkContextAttribute"/>,
    /// if is decorated with this attribute, will have effect.
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public class SinkMethodAttribute : Attribute
    {
        private readonly SinkContent _content;

        public SinkMethodAttribute(SinkContent content)
        {
            _content = content;
        }

        public string LogKey { get; set; }
        public string VerificationKey { get; set; }
        public string ExtensionKey { get; set; }

        public SinkContent Content => _content;
    }

    [Flags]
    public enum SinkContent
    {
        None = 1,
        Log = 1 << 1,
        Verify = 1 << 2,
        Extend = 1 << 3,
    }
}
