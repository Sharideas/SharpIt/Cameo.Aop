﻿namespace Cameo.Aop
{
    /// <summary>
    /// A class only deriving from this interface can do log. Also need decorated with <see cref="SinkContextAttribute"/>.
    /// If only a method or a property decorated with <see cref="SinkMethodAttribute"/>, and the sink content include <see cref="SinkContent.Log"/>,
    /// will has effect.
    /// </summary>
    public interface IHasLogger
    {
        ICustomLogger Logger { get; }
    }

    /// <summary>
    /// There is no given deriving type from this interface.
    /// You should derive this interface by yourself, and make the method will do log.
    /// </summary>
    public interface ICustomLogger : ICustomPart
    {
        /// <summary>
        /// Append a log with working level.
        /// </summary>
        /// <param name="content">Log content string</param>
        void Log(string content);

        /// <summary>
        /// Append a log with given level. If given level is lower than working level, this method should not log.
        /// </summary>
        /// <param name="content">Log content string</param>
        /// <param name="level">Level the content should be append with.</param>
        void Log(string content, LogLevel level);
    }

    public enum LogLevel
    {
        Current = -1,
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
    }
}
