﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using Cameo.Aop;

namespace OldTree.Infrastructure.Attributes.AopLog
{
    internal class MethodSinker : IMessageSink
    {
        private static List<int> _logBlockList = new List<int>();

        private MarshalByRefObject _obj;

        public MethodSinker(MarshalByRefObject obj, IMessageSink nextSink)
        {
            _obj = obj;
            NextSink = nextSink;
        }

        public IMessageSink NextSink { get; private set; }

        public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
        {
            var id = Thread.CurrentThread.ManagedThreadId;
            if (!_logBlockList.Contains(id))
            {
                if (msg is IMethodCallMessage methodCall && Attribute.GetCustomAttribute(methodCall.MethodBase, typeof(SinkMethodAttribute)) is SinkMethodAttribute attr && attr != null)
                {
                    try
                    {
                        //TODO: Verify should judge, if failed, break the method.
                        verify(attr, methodCall, before);
                        _logBlockList.Add(id);
                        log(attr, methodCall, before);
                        extension(attr, methodCall, before);
                    }
                    catch { }
                    var result = NextSink.AsyncProcessMessage(msg, replySink);
                    try
                    {
                        if (result is IMethodReturnMessage methodReturn)
                        {
                            if (methodReturn.Exception != null)
                            {
                                log(attr, methodReturn, exception);
                                extension(attr, methodCall, exception);
                            }
                            else
                            {
                                log(attr, methodReturn, after);
                                extension(attr, methodCall, after);
                            }
                        }
                    }
                    catch { }
                    _logBlockList.RemoveAll((c) => c == id);
                    return result;
                }
            }

            return NextSink.AsyncProcessMessage(msg, replySink);
        }

        public IMessage SyncProcessMessage(IMessage msg)
        {
            var id = Thread.CurrentThread.ManagedThreadId;
            if (!_logBlockList.Contains(id))
            {
                if (msg is IMethodCallMessage methodCall && Attribute.GetCustomAttribute(methodCall.MethodBase, typeof(SinkMethodAttribute)) is SinkMethodAttribute attr && attr != null)
                {
                    try
                    {
                        //TODO: Verify should judge, if failed, break the method.
                        verify(attr, methodCall, before);
                        _logBlockList.Add(id);
                        log(attr, methodCall, before);
                        extension(attr, methodCall, before);
                        _logBlockList.RemoveAll((c) => c == id);
                    }
                    catch { }
                    var result = NextSink.SyncProcessMessage(msg);
                    _logBlockList.Add(id);
                    try
                    {
                        if (result is IMethodReturnMessage methodReturn)
                        {
                            if (methodReturn.Exception != null)
                            {
                                log(attr, methodReturn, exception);
                                extension(attr, methodCall, exception);
                            }
                            else
                            {
                                log(attr, methodReturn, after);
                                extension(attr, methodCall, after);
                            }
                        }
                    }
                    catch { }
                    _logBlockList.RemoveAll((c) => c == id);
                    return result;
                }
            }

            return NextSink.SyncProcessMessage(msg);
        }

        private void log(SinkMethodAttribute attr, IMethodMessage method, Action<ICustomLogger, SinkMethodAttribute, IMethodMessage> logger)
        {
            if ((attr.Content | SinkContent.Log) == 0 || !(_obj is IHasLogger l))
            {
                return;
            }

            logger(l.Logger, attr, method);
        }

        private void verify(SinkMethodAttribute attr, IMethodMessage method, Action<ICustomVerifier, SinkMethodAttribute, IMethodMessage> verifier)
        {
            if ((attr.Content | SinkContent.Log) == 0 || !(_obj is IHasVerifier v))
            {
                return;
            }

            verifier(v.Verifier, attr, method);
        }

        private void extension(SinkMethodAttribute attr, IMethodMessage method, Action<ICustomExtension, SinkMethodAttribute, IMethodMessage> extender)
        {
            if ((attr.Content | SinkContent.Log) == 0 || !(_obj is IHasExtension e))
            {
                return;
            }

            extender(e.Extension, attr, method);
        }

        private void before(ICustomPart part, SinkMethodAttribute attr, IMethodMessage method)
        {
            if (method is IMethodCallMessage call)
            {
                part?.DoBeforeMethodCall?.Invoke(call, attr.LogKey);
            }
        }

        private void exception(ICustomPart part, SinkMethodAttribute attr, IMethodMessage method)
        {
            if (method is IMethodReturnMessage ret)
            {
                part?.DoWhenThrowExcception?.Invoke(ret, attr.LogKey);
            }
        }

        private void after(ICustomPart part, SinkMethodAttribute attr, IMethodMessage method)
        {
            if (method is IMethodReturnMessage ret)
            {
                part?.DoAfterMethodCall?.Invoke(ret, attr.LogKey);
            }
        }
    }
}