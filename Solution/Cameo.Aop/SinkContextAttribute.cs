﻿using System;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;
using OldTree.Infrastructure.Attributes.AopLog;

namespace Cameo.Aop
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class SinkContextAttribute : ContextAttribute, IContributeObjectSink
    {
        public SinkContextAttribute() : base(nameof(SinkContextAttribute))
        {
        }

        public IMessageSink GetObjectSink(MarshalByRefObject obj, IMessageSink nextSink) => new MethodSinker(obj, nextSink);
    }
}
